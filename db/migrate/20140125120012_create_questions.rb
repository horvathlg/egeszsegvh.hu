class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :user_id
      t.text :body
      t.boolean :answered, default: false

      t.timestamps
    end
    add_index :questions, :user_id
  end
end
