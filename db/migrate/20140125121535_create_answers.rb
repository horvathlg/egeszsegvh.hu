class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :question_id
      t.string :user_id
      t.string :body
      t.integer :points
      t.boolean :good_answer, default: false

      t.timestamps
    end
    add_index :answers, :question_id
    add_index :answers, :user_id
  end
end
