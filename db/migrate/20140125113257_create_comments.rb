class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :user_id
      t.string :article_id
      t.text :body
      t.datetime :published_at

      t.timestamps
    end

    add_index :comments, :article_id
    add_index :comments, :user_id

  end
end
