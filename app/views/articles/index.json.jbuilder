json.array!(@articles) do |article|
  json.extract! article, :id, :title, :excerpt, :body
  json.url article_url(article, format: :json)
end
