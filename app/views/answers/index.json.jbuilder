json.array!(@answers) do |answer|
  json.extract! answer, :id, :question_id, :user_id, :body, :points, :good_answer
  json.url answer_url(answer, format: :json)
end
