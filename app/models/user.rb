class User < ActiveRecord::Base
	attr_accessor :login

	has_many :comments
	has_many :questions
	has_many :answers
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
