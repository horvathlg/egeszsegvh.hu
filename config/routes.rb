EgeszsegvhHu::Application.routes.draw do

  resources :questions do
    resources :users
    resources :answers
  end

  resources :articles do
    resources :users
    resources :comments
  end

  devise_for :users, :path_names => { :sign_up => "register", :sign_in => "login", :sign_out => "logout"}
  
  root :to => "home#index"

end
